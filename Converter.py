from string import ascii_letters
from string import punctuation
from string import digits
from CharacterAnalyzer import CharacterAnalyzer

# TODO: test accuracy of rounding to 0.40, i.e. 1 char represents 2x5px


class Converter:
    """Handles actual conversion of the image to ascii characters"""

    def __init__(self, image):
        self.image_to_convert = image
        self.ASCII_CHARS = list(ascii_letters + punctuation + digits)
        self.filepath_for_export = '/Users/callumdavies/PycharmProjects/asciify/Exports/Output.txt'

    def do_the_conversion(self):
        """Maps each pixel to a character based on the range of its brightness value (0-255).
        0-255 is divided into an amount of equal ranges equal to self.ASCII_CHARS' length"""

        character_weights = CharacterAnalyzer(self.ASCII_CHARS).calculate_weights()
        # print("Character weights: {}".format(character_weights))

        pixels = self.image_to_convert.load()
        range_width = int(round(256/len(self.ASCII_CHARS)))  # 256 brightness values / 94 ASCII_CHARS = 1 character representing ~3 brightness values

        with open(self.filepath_for_export, 'w') as fp:
            for y in range(0, self.image_to_convert.size[1], 2):
                for x in range(self.image_to_convert.size[0]):

                    # Most font aspect ratios are ~0.5 so convert 2 rows to 1 char to prevent vertical stretching
                    start_pixel = pixels[x, y]  # averages current pixel's brightness with pixel below
                    if y == self.image_to_convert.size[1]:
                        mod_pixel = pixels[x, y]
                    else:
                        mod_pixel = pixels[x, y + 1]

                    averaged_pixel = float((start_pixel + mod_pixel)/2)
                    pixel_range = int(averaged_pixel / range_width)  # e.g. 2, which = 2nd highest brightness range (252, 251, 250)
                    this_character_weight = character_weights[pixel_range-1]

                    fp.write(this_character_weight[0])
                fp.write("\n")
            print("Complete")