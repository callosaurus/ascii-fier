from PIL import ImageFont
import operator


class CharacterAnalyzer:
    """Sorts list of characters according to Density
    Density of a character = ratio between the white and black pixels in a rasterized version of the character
    Merge sorts characters based on their density ( O(nlogn) )"""

    def __init__(self, characters):
        self.characters = characters

    def calculate_weights(self):
        """calculate and sort the 'weights' of ASCII self.characters"""

        font = ImageFont.load_default()  # load default bitmap monospaced font
        (character_x, character_y) = font.getsize(chr(32))

        weights_dict = {}
        for item in range(0, len(self.characters)):
            character_image = font.getmask(self.characters[item])
            count = 0
            for y in range(character_y):
                for x in range(character_x):
                    if character_image.getpixel((x, y)) > 0:
                        count += 1
            this_character = self.characters[item]
            # print("{}".format(this_character))
            weights_dict[this_character] = (float(count) / (character_x * character_y))

        # sorted_weights_dict = [(k, v) for k, v in weights_dict.items()]
        sorted_weights = sorted(weights_dict.items(), key=operator.itemgetter(1))

        return sorted_weights
