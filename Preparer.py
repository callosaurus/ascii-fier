from PIL import Image
from Converter import Converter


class Preparer:
    """Crops/resizes an image if needed"""

    def __init__(self, image, mode):
        self.imageToResize = image
        self.colourMode = mode

    def do_the_prep(self):
        self.scale_image_with_width(500)
        self.imageToResize = self.convert_to_grayscale(self.imageToResize)
        self.pass_resized_image_to_converter()

    def convert_to_grayscale(self, image):
        image = image.convert('L')
        print("Converted to grayscale")
        return image

    def scale_image_with_width(self, new_width):
        # Resizes an image preserving the aspect ratio. The closer to the original the better.
        # Atom displays 501 characters-per-line before cutting off.

        (original_width, original_height) = self.imageToResize.size
        aspect_ratio = original_height / float(original_width)
        new_height = int(aspect_ratio * new_width)

        self.imageToResize = self.imageToResize.resize((new_width, new_height))

    def pass_resized_image_to_converter(self):
        Converter(self.imageToResize).do_the_conversion()
