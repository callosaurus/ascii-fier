from InputHandler import InputHandler
from Preparer import Preparer
from Converter import Converter
from CharacterAnalyzer import CharacterAnalyzer
from PIL import Image


def test_input_handler_does_not_open_bad_file():
    ip = InputHandler()
    ip.open_file_at_path('/')
    assert (ip.openedImage is None)


def test_input_handler_opens_image_correctly():
    handler = InputHandler()
    handler.open_file_at_path('/Users/callumdavies/ascii-fier/Resources/random_flowerlion_for_sale___close_by_reykat-d9bihhg.jpg')
    assert (type(handler.openedImage) is not None)


def test_preparer_resizes_properly():
    inp = InputHandler()
    inp.open_file_at_path('/Users/callumdavies/ascii-fier/Resources/random_flowerlion_for_sale___close_by_reykat-d9bihhg.jpg')
    img = inp.openedImage
    prep = Preparer(img,'grayscale')
    im = prep.imageToResize
    assert (im.size[0] == 500)