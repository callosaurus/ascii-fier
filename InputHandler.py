from PIL import Image
from Preparer import Preparer
from urllib import request
from io import BytesIO

# TODO's:
# mergesort ASCII chars?
# desiredColourMode = input("Grayscale or colour?")
# gif conversion support


class InputHandler:
    """For asking user input and saving initial user options"""

    def __init__(self):
        self.openedImage = None

    def open_file_at_path(self, image_source):
        if image_source.startswith("http://") or image_source.startswith("https://"):
            try:
                url_contents = request.urlopen(image_source)
                f = BytesIO(url_contents.read())
                self.openedImage = Image.open(f)
            except Exception as e:
                print("ERROR: Unable to open image file from url:{url}".format(url=image_source))
                print("Exception: \n{exception}".format(exception=e))
                return
        else:
            try:
                self.openedImage = Image.open(image_source)
            except Exception as e:
                print("ERROR: Unable to open image file at path:{image_filepath}".format(image_filepath=image_source))
                print("Exception: \n{exception}".format(exception=e))
                return

    def get_user_input(self):
        filepath = input("URL/Filepath of the image to convert?: ")
        self.open_file_at_path(filepath)
        self.pass_user_input_options_to_preparer()

    def pass_user_input_options_to_preparer(self):
        Preparer(self.openedImage,'grayscale').do_the_prep()
