# https://www.hackerearth.com/practice/notes/beautiful-python-a-simple-ascii-art-generator-from-images/
from PIL import Image
from InputHandler import InputHandler
from string import ascii_letters
from string import punctuation
from string import digits

# ASCII_CHARS = ['#', '?', '%', '.', 'S', '+', '.', '*', ':', ',', '@']
# ASCII_CHARS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u'
# , 'v', 'w', 'x', 'y', 'z']
ASCII_CHARS = list(ascii_letters + punctuation + digits)


def scale_image(image, new_width=500):
    # Resizes an image preserving the aspect ratio, the closer to the original the better.
    # Atom displays 500 characters before cutting off.

    (original_width, original_height) = image.size
    aspect_ratio = original_height/float(original_width)
    new_height = int(aspect_ratio * new_width)

    new_image = image.resize((new_width, new_height))
    return new_image


def convert_to_grayscale(image):
    return image.convert('L')


def map_pixels_to_ascii_chars(image, range_width=3):
    # Maps each pixel to an ascii char based on the range in which it lies.
    # 0-255 is divided into 94 ranges (length of ASCII_CHARS) of 3 pixels each.

    new_width = int(round(256/len(ASCII_CHARS)))
    print("new_width: {}".format(new_width))

    pixels_in_image = list(image.getdata())
    pixels_to_chars = [ASCII_CHARS[int(pixel_value/range_width)] for pixel_value in pixels_in_image]

    # joins pixels
    return "".join(pixels_to_chars)


def convert_image_to_ascii(image, new_width=500):
    image = scale_image(image)
    image = convert_to_grayscale(image)

    pixels_to_chars = map_pixels_to_ascii_chars(image)
    len_pixels_to_chars = len(pixels_to_chars)

    image_ascii = [pixels_to_chars[index: index + new_width] for index in range(0, len_pixels_to_chars, new_width)]
    # print("IMAGE ASCII BEFORE JOINING: {}".format(image_ascii))
    return "\n".join(image_ascii)


def handle_image_conversion(image_filepath):
    image = None
    try:
        image = Image.open(image_filepath)
    except Exception as e:
        print("ERROR: Unable to open image file {image_filepath}".format(image_filepath=image_filepath))
        print("Exception: \n{exception}".format(exception=e))
        return

    image_ascii = convert_image_to_ascii(image)
    print(image_ascii)


if __name__ == '__main__':

    # image_file_path = '/Users/callumdavies/PycharmProjects/asciify/Resources/random_flowerlion_for_sale___close_by_reykat-d9bihhg.jpg'
    # image_file_path = '/Users/callumdavies/PycharmProjects/asciify/Resources/test_bw_halves.png'
    # handle_image_conversion(image_file_path)
    handler = InputHandler().get_user_input()
